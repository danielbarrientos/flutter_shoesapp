import 'package:flutter/material.dart';

class ShoeSizePreview extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 23, vertical: 5 ),
      child: Container(
        width: double.infinity,
        height: 430,
        decoration: BoxDecoration(
          color: Color(0xffFFCF53),
          borderRadius: BorderRadius.circular(50)
        ),
       child: Column(
         children: [
           _ShoeWithShadow(),
           _ShoeSize(),
         ],
       ),
      ),
    );
  }
}

class _ShoeSize extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _ShoeSizeBox(7),
          _ShoeSizeBox(7.5),
          _ShoeSizeBox(8),
          _ShoeSizeBox(8.5),
          _ShoeSizeBox(9),
          _ShoeSizeBox(9.5),
        ],
      ),
    );
  }
}

class _ShoeSizeBox extends StatelessWidget {

  final double size;

  const _ShoeSizeBox( this.size);
 
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Text(
        '${size.toString().replaceAll('.0','')}',
        style: TextStyle(color: (this.size == 9) ?Colors.white :Color(0xffF1A23A),fontSize: 16,fontWeight: FontWeight.bold ),
      ),
      width: 45,
      height: 45,
      decoration: BoxDecoration(
        color: (this.size == 9)? Color(0xffF1A23A):Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          if(this.size == 9)
            BoxShadow(color: Color(0xffF1A23A), blurRadius: 10,offset: Offset(0,5))
        ]
      ),
    );
  }
}

class _ShoeWithShadow extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: Stack(
        children: [

          Positioned(
            bottom: 15,
            right: 0,
            child: _Shadow()
          ),
          Image(image: AssetImage('assets/imgs/azul.png'),)
        ],
      ),
    );
  }
}

class _Shadow extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: -0.6,
      child: Container(
        width: 230,
        height: 130,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          boxShadow: [
            BoxShadow(color: Color(0xffEAA14E), blurRadius: 40)
          ]
        ),
      ),
    );
  }
}